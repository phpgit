<?php

require_once dirname(__FILE__) . '/Git/Lazy.php';
require_once dirname(__FILE__) . '/Git/Actor.php';
require_once dirname(__FILE__) . '/Git/Commit.php';
require_once dirname(__FILE__) . '/Git/Exception.php';
require_once dirname(__FILE__) . '/Git/Repo.php';
require_once dirname(__FILE__) . '/Git/Tree.php';
require_once dirname(__FILE__) . '/Git/Blob.php';

/**
 * Wrapper for Git executable. Lowest level interface.
 */
class Git
{

    const S = DIRECTORY_SEPARATOR;

    static protected $executeKwargs = array(
        'istream', 'withKeepCwd', 'withExtendedOutput',
        'withExceptions', 'withRawOutput'
    );

    /**
     * Git executable to invoke. You can replace this with a full path
     * to your Git executable/wrapper script, but be sure to escape
     * everything properly!
     */
    static public $git = 'git';

    /**
     * Current working directory.
     */
    protected $dir;

    /**
     * @param $dir Current working directory.
     */
    public function __construct($dir) {
        $this->dir = $dir;
    }

    /**
     * Returns current working directory.
     */
    public function getDir() {return $this->dir;}

    // GitPython appears to have an implementation of getting
    // attributes which also calls process. We're only going to support
    // pure method calls... for now.
    // public function __get($name)

    /**
     * Executes a command on shell and returns output.
     * @warning $istream is a STRING not a HANDLE, as it is in the Python 
     *      implementation. We might want to change this some time.
     * @param $command Command argument list to handle.
     * @param $istream Stdin string passed to subprocess.
     * @param $options Lookup array of options. These options are:
     *      'withKeepCwd' => Whether to use current working directory from
     *          getcwd() or the Git directory in $this->dir
     *      'withExtendedOutput' => Whether to return array(status, stdout, stderr)
     *      'withExceptions' => Whether to raise an exception if Git returns
     *          a non-zero exit status
     *      'withRawOutput' => Whether to avoid stripping off trailing whitespace
     * @return String stdout output when withExtendedOutput is false, see above
     *      if true.
     */
    public function execute($command, $istream = null, $options = array()) {
        if (is_array($command)) {
            foreach ($command as &$c) $c = escapeshellarg($c);
            $command = implode(' ', $command);
        }
        //var_dump($command);
        $options = array_merge(array(
            'withKeepCwd' => false,
            'withExtendedOutput' => false,
            'withExceptions' => true,
            'withRawOutput' => false,
        ), $options);
        if ($options['withKeepCwd'] || is_null($this->dir)) {
            $cwd = getcwd();
        } else {
            $cwd = $this->dir;
        }
        if (strtoupper(substr(PHP_OS, 0, 3)) == 'WIN') {
            // Windows dependent code, stolen from PHPT
            $com = new COM('WScript.Shell');
            $com->CurrentDirectory = $cwd;
            $proc = $com->Exec($command);
            if (!is_null($istream)) $proc->StdIn->Write($istream);
            $stdout = $proc->StdOut->ReadAll();
            $stderr = $proc->StdErr->ReadAll();
            $status = $proc->ExitCode;
        } else {
            // untested! Also stolen from PHPT
            // this seems needlessly complicated
            $pipes_template = array(
                0 => array('pipe', 'r'),
                1 => array('pipe', 'w'),
                2 => array('pipe', 'w'),
                3 => array('pipe', 'w'), // pipe to write exit code to
            );
            $pipes = array();
            $proc = proc_open($command, $pipes_template, $pipes);
            // Input
            fwrite($pipes[0], $istream);
            fclose($pipes[0]);
            // Output
            $stdout = stream_get_contents($pipes[1]);
            fclose($pipes[1]);
            // Error
            $stderr = stream_get_contents($pipes[2]);
            fclose($pipes[2]);
            // Status
            $status = trim(fread($pipes[3], 5));
            fclose($pipes[3]);
            // Cleanup
            $close = proc_close($proc);
            if (empty($code)) $status = $close;
        }
        // We want $stderr, $stdout and $status
        if (!$options['withRawOutput']) {
            // This feels buggy to me, especially with git cat-file
            $stdout = rtrim($stdout);
            $stderr = rtrim($stderr);
        }
        if ($options['withExceptions'] && $status !== 0) {
            throw new Git_Exception_Command($command, $status, $stderr);
        }
        // Trace code omitted
        if ($options['withExtendedOutput']) {
            return array($status, $stdout, $stderr);
        } else {
            return $stdout;
        }
    }

    /**
     * Transforms an associative array of arguments to command line options.
     * Arguments can be like 'r' or 'no-commit'.
     * @note Original Python version kwargs used 'no_commit'
     *       form due to Python conventions. We decided to use a more direct
     *       approach because our associative array allow them.
     */
    public function transformArgs($kwargs) {
        $args = array();
        foreach ($kwargs as $k => $v) {
            if (strlen($k) == 1) {
                if ($v === true) $args[] = "-$k";
                else $args[] = "-$k$v";
            } else {
                // $k = dashify($k);
                if ($v === true) $args[] = "--$k";
                else $args[] = "--$k=$v";
            }
        }
        return $args;
    }

    /**
     * Runs a given Git command with the specified arguments and return
     * the result as a string.
     * @param $method Name of command, but camelCased instead of dash-ified.
     * @param $args Array of arguments. There is actually only one argument,
     *        which is an array of associative and numerically indexed
     *        parameters.
     * @return String output.
     */
    public function __call($method, $raw_args) {
        // split out "kwargs" (to be converted to options) from regular
        // "args" (which get inserted normally)
        $args = array();
        $kwargs = array();
        foreach ($raw_args as $raw) {
            if (is_array($raw)) $kwargs = $raw; // only one assoc array allowed!
            else $args[] = $raw;
        }
        for ($i = 0; isset($kwargs[$i]); $i++) {
            $args[] = $kwargs[$i];
            unset($kwargs[$i]);
        }
        // Grab "special" arguments prior to transformation
        $executeKwargs = array();
        foreach ($kwargs as $k => $v) {
            if (isset(self::$executeKwargs[$k])) {
                $executeKwargs[$k] = $v;
                unset($kwargs[$k]);
            }
        }
        // $args and $kwargs are interesting
        $opt_args = $this->transformArgs($kwargs);
        // parse through $args again to determine which ones are actually kwargs
        $ext_args = $args;
        // Full arguments
        $args = array_merge($opt_args, $ext_args);
        // Convert methodName to method-name (our equivalent of dashify).
        // This is kind of inefficient.
        $command = '';
        for ($i = 0, $max = strlen($method); $i < $max; $i++) {
            $c = $method[$i];
            $command .= ctype_upper($c) ? '-' . strtolower($c) : $c;
        }
        $call = array_merge(array(self::$git, $command), $args);
        $result = $this->execute($call);
        return $result;
    }

}
