<?php

/**
 * Represents a tree file structure in Git.
 */
class Git_Tree extends Git_Lazy {
    /**
     * Instance of Git_Repo.
     */
    public $repo;
    /**
     * Treeish identifier of the tree. Can be a SHA-1 sum.
     */
    protected $id;
    protected $mode;
    protected $name;
    protected $contents;
    public function __construct($repo, $kwargs = array()) {
        $this->repo = $repo;
        foreach ($kwargs as $k => $v) {
            $this->$k = $v;
        }
    }
    /**
     * Lazy loads the tree's contents.
     */
    protected function bake() {
        $tmp = Git_Tree::construct($this->repo, $this->id);
        $this->contents = $tmp->contents;
    }
    /**
     * Constructs and fully initializes a tree.
     */
    public static function construct($repo, $treeish, $paths = array()) {
        $output = $repo->git->__call('lsTree', array_merge($paths, array($treeish)));
        $tree = new Git_Tree($repo, array('id' => $treeish));
        $tree->constructInitialize($repo, $treeish, $output);
        return $tree;
    }
    /**
     * Initializes a tree object based on the output of the ls-tree command.
     */
    public function constructInitialize($repo, $id, $text) {
        $this->repo = $repo;
        $this->id = $id;
        $this->contents = array();
        foreach (explode("\n", $text) as $line) {
            $out = self::contentFromString($repo, $line);
            if (!$out) continue;
            $this->contents[] = $out;
        }
        $this->markBaked();
    }
    /**
     * Creates a content object (blob or tree) based on the ls-tree command.
     */
    public function contentFromString($repo, $text) {
        $text = str_replace("\t", ' ', $text);
        $bits = explode(' ', $text);
        if (count($bits) != 4) return;
        list($mode, $type, $id, $name) = $bits;
        switch ($type) {
            case 'tree':
                return new Git_Tree($repo, compact('id', 'mode', 'name'));
            case 'blob':
                return new Git_Blob($repo, compact('id', 'mode', 'name'));
            case 'commit':
                return;
            default:
                throw new Git_Exception('Invalid type: ' . $type);
        }
    }
    /**
     * Retrieves a named object in this tree's contents.
     * @param Filename of object to return. This function does NOT search
     *        recursively.
     * @return Null if file not found, otherwise Git_Tree or Git_Commit file.
     */
    public function get($file) {
        foreach ($this->contents as $c) {
            if ($c->name == $file) return $c;
        }
    }
    /**
     * Retrieves the basename of this tree.
     */
    public function basename() {
        return basename($this->__get('name'));
    }
    public function __toString() {
        return '(Git_Tree "'.$this->id.'")';
    }
}
