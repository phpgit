<?php

/**
 * Represents a Git repository, and allows access to history, snapshots,
 * commits, et cetera.
 */
class Git_Repo {

    const DAEMON_EXPORT_FILE = 'git-daemon-export-ok';

    /**
     * Path to this repository.
     */
    public $path;

    /**
     * Whether or not this is a bare repository.
     */
    public $bare;

    /**
     * Interface to Git itself.
     */
    public $git;

    /**
     * Current working directory.
     */
    public $wd;

    /**
     * @param $path Path to the repository (either working copy or bare)
     */
    public function __construct($path = null) {
        $path = $path ? $path : getcwd();
        $path = realpath($path); // I believe PHP automatically expands ~
        if ($path === false) {
            throw new Git_Exception("No such path '$path'");
        }
        $curpath = $path;
        while ($curpath) {
            if ($this->isGitDir($curpath)) {
                $this->bare = true;
                $this->path = $this->wd = $curpath;
            }
            $gitpath = "$curpath/.git";
            if ($this->isGitDir($gitpath)) {
                $this->bare = false;
                $this->path = realpath($gitpath);
                $this->wd = $curpath;
                break;
            }
            $temp = dirname($curpath);
            if (!$temp || $temp == $curpath) break;
            $curpath = $temp;
        }
        if (is_null($this->path)) throw new Git_Exception_InvalidRepository($path);
        $this->git = new Git($this->wd);
    }

    /**
     * Determines whether or not this is a Git directory.
     * @note Taken from setup.c:is_git_directory
     */
    protected function isGitDir($d) {
        if (is_dir($d) && is_dir("$d/objects") && is_dir("$d/refs")) {
            $headref = "$d/HEAD";
            return is_file($headref) || 
                (is_link($headref) && strncmp(readlink($headref), 'refs', 4));
        } else return false;
    }

    // * means will implement soon

    // public function description() {
    // * public function heads() {
    // * public function branches() {return $this->heads();}
    // public function tags() {
    // * public function commits($start, $max_count, $skip) {
    // public function commitsBetween($frm, $to) {
    // public function commitsSince($start = 'master', $since) {
    // public function commitCount($start = 'master') {
    // * public function commit($id) {
    // public function commitDeltasFrom($other_repo, $ref = 'master', $other_ref = 'master') {
    /**
     * Returns the Git_Tree object for a given treeish reference.
     * @param $treeish Reference to retrieve, can be branch name, or filename
     * @param $paths Optional array of directory paths to restrict to.
     * @return Git_Tree for that path.
     */
    public function tree($treeish = 'master', $paths = array()) {
        return Git_Tree::construct($this, $treeish, $paths);
    }
    // * public function blob($id) {

    /**
     * Returns the commit log for a treeish entity.
     * @param $commit Commit to get log of, or commit range like begin..end
     * @param $path Path (or paths) to get logs for
     * @param $kwargs Extra arguments, see Git->transformArgs()
     */
    public function log($commit = 'master', $path = array(), $kwargs = array()) {
        $options = array_merge(array('pretty' => 'raw'), $kwargs);
        if ($path) {
            $arg = array_merge(array($commit, '--'), $path);
        } else {
            $arg = array($commit);
        }
        // This call is kinda weird, I know, but otherwise we have
        // to do user_func_call_array. Perhaps we should patch __call
        // to work with numeric arrays? A thought...
        $commits = $this->git->__call('log', array_merge($arg, array($options)));
        return Git_Commit::listFromString($this, $commits);
    }

    // public function diff($a, $b, $paths = array()) {
    // public function commitDiff($commit) {
    // public static function initBare($path, $kwargs) {
    // public static function forkBare($path, $kwargs) {
    // public function archiveTar($treeish = 'master', $prefix = null) {
    // public function archiveTarGz($treeish = 'master', $prefix = null) {
    // public function enableDaemonServe() {
    // public function disableDaemonServe() {
    // ? private function _getAlternates() {
    // ? private function _setAlternates() {

    public function __toString() {
        return '(Git_Repo "'. $this->path .'")';
    }

}
