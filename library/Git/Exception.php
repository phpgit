<?php

class Git_Exception extends Exception
{
    
}

class Git_Exception_InvalidRepository extends Git_Exception
{
    public function __construct($path) {
        parent::__construct("Git repository $path does not exist");
    }
}

class Git_Exception_Command extends Git_Exception
{
    public function __construct($command, $status, $stderr) {
        parent::__construct("Command `$command` returned error code [$status] with message \"$stderr\"");
    }
}
