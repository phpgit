<?php

/**
 * Implements lazy loading of properties.
 *
 * In the Python implementation, this is implemented as a
 * mix-in. However, we don't have this capability in PHP, so
 * we implement is as a super-class (as multiple mix-ins
 * are not used). If this ends up being a problem, rewrite
 * this class to be a decorator, and create a factory for
 * Git objects.
 * 
 * @warning Subclasses should be careful to call __get manually
 *          if they're not sure if a particular property will
 *          be initialized or not; because they have full access
 *          __get will not be called automatically.
 */
abstract class Git_Lazy {
    /**
     * Whether or not this class's contents have been loaded.
     */
    private $_baked = false;
    /**
     * Marks the class as baked; use this if you loaded the
     * contents yourself.
     */
    protected function markBaked() {
        $this->_baked = true;
    }
    /**
     * Implements public lazy loading.
     */
    public function __get($name) {
        if ($name[0] == '_') throw new Git_Exception('Cannot access private property ' . $name);
        // Quick and easy way of emulating Python's @property decorator
        if (method_exists($this, $name)) {
            return $this->$name();
        }
        if (isset($this->$name)) return $this->$name;
        if (!$this->_baked) {
            $this->bake();
            $this->_baked = true;
        }
        return $this->$name;
    }
    /**
     * Implements the loading operation; after this method is
     * called the class should be ready to go.
     */
    abstract protected function bake();
}
