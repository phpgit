<?php

/**
 * Represents a binary large object in Git, usually a file in a tree.
 */
class Git_Blob
{
    const DEFAULT_MIME_TYPE = 'text/plain';
    /**
     * Instance of Git_Repo.
     */
    public $repo;
    public $id;
    public $mode;
    public $name;
    private $_size;
    private $_data;
    /**
     * Creates an unbaked Blob containing just the specified attributes.
     */
    public function __construct($repo, $kwargs) {
        $this->repo = $repo;
        foreach ($kwargs as $k => $v) {
            $this->$k = $v;
        }
    }
    /**
     * Size of this blob in bytes.
     */
    public function size() {
        if (is_null($this->_size)) {
            $this->_size = (int) $this->repo->git->catFile($this->id, array('s' => true));
        }
        return $this->_size;
    }
    /**
     * Returns the binary contents of this blob.
     */
    public function data() {
        if (is_null($this->_data)) {
            $this->_data = $this->repo->git->catFile($this->id, array('p' => true));
        }
        return $this->_data;
    }
    // This function isn't implemented because we don't really have a
    // good MIME implementation by default on PHP. Will implement after
    // more investigation.
    // public function mimeType() {
    /**
     * Basename of this blob.
     */
    public function basename() {
        return basename($this->__get('name'));
    }
    // public static function blame($repo, $commit, $file) {
    public function __toString() {
        return '(Git_Blob "' . $this->id . '")';
    }
    public function __get($name) {
        if (method_exists($this, $name)) return $this->$name();
        else return $this->$name;
    }
}
