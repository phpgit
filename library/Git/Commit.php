<?php

/**
 * Represents a commit in the Git repository.
 */
class Git_Commit {

    public $repo, $id, $tree, $author, $authoredDate, $committer,
        $committedDate, $message, $parents = array();

    /**
     * @param $repo Git_Repo of this commit.
     * @param $kwargs Hash of info about this commit, specifically:
     *      'id' => Id of commit
     *      'parents' => List of commit IDs (converted to Git_Commit objects)
     *      'tree' => Tree ID (converted to Git_Tree object)
     *      'author' => Author string
     *      'authoredDate' => Authored DateTime
     *      'committer' => Committer string
     *      'committedDate' => Committed DateTime
     *      'message' => First line of commit message
     */
    public function __construct($repo, $kwargs) {
        $this->repo = $repo;
        foreach ($kwargs as $k => $v) {
            if ($k == 'parents') {
                foreach ($v as $id) {
                    $this->parents[] = new Git_Commit($repo, array('id' => $id));
                }
                continue;
            } elseif ($k == 'tree') {
                $this->tree = new Git_Tree($repo, array('id' => $v));
                continue;
            }
            $this->$k = $v;
        }
    }

    // __bake__

    /**
     * Return a shortened representation of Git's commit ID.
     */
    public function idAbbrev() {
        return substr($this->id, 0, 7);
    }

    // public static(?) function count() {
    // public static function findAll($repo, $ref, $kwargs) {

    /**
     * Parses out commit information from git log --pretty raw into an
     * array of Commit objects.
     * @param $repo Git_Repo
     * @param $text Text from command
     * @return Array of Git_Commit objects
     */
    public static function listFromString($repo, $text) {
        $lines = explode("\n", $text);
        foreach ($lines as $k => $v) if (trim($v) === '') unset($lines[$k]);
        $lines = array_values($lines);

        $commits = array();
        for ($i = 0, $c = count($lines); $i < $c;) {

            $id   = self::_l($lines, $i, true);
            $tree = self::_l($lines, $i, true);

            $parents = array();
            while ($i < $c && strncmp($lines[$i], 'parent', 6) === 0) {
                $parents[] = self::_l($lines, $i, true);
            }
            list($author, $authoredDate) = self::actor(self::_l($lines, $i));
            list($committer, $committedDate) = self::actor(self::_l($lines, $i));

            $messages = array();
            while ($i < $c && strncmp($lines[$i], '    ', 4) === 0) {
                $messages[] = trim(self::_l($lines, $i));
            }

            $message = $messages ? $messages[0] : '';

            $commits[] = new Git_Commit($repo, compact(
                'id', 'parents', 'tree', 'author', 'authoredDate',
                'committer', 'committedDate', 'message'
            ));
        }
        return $commits;
    }

    /**
     * Grabs the current line, advances the index forward, and parses
     * out the last bit.
     * @param $lines Array of lines
     * @param &$i Index in $lines array
     * @param $grab_last Whether or not to retrieve the span of text after
     *        the last whitespace.
     */
    private static function _l($lines, &$i, $grab_last = false) {
        $line = $lines[$i++];
        if ($grab_last) {
            $line = trim($line);
            $line = substr($line, strrpos($line, ' ') + 1);
        }
        return $line;
    }

    /**
     * Parse out actor (author/committer) information.
     * @returns array('Actor name <email>', timestamp)
     */
    public static function actor($line) {
        preg_match('/^.+? (.*) (\d+) .*$/', $line, $matches);
        list($x, $actor, $epoch) = $matches;
        return array(Git_Actor::fromString($actor), new DateTime('@' . $epoch));
    }

}
