<?php

/**
 * An actor, either a committer or an author.
 */
class Git_Actor {

    /**
     * Name of actor.
     */
    public $name;

    /**
     * Email of actor.
     */
    public $email;

    public function __construct($name, $email = null) {
        $this->name = $name;
        $this->email = $email;
    }
    public function __toString() {
        return $this->name;
    }

    /**
     * Parses a string from John Doe <jdoe@example.com> to this object.
     */
    public static function fromString($string) {
        if (preg_match('/(.*) <(.+?)>/', $string, $matches)) {
            list($x, $name, $email) = $matches;
            return new Git_Actor($name, $email);
        } else {
            return new Git_Actor($string);
        }
    }
}
