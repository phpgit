<?php

error_reporting(E_ALL | E_STRICT);

chdir(dirname(__FILE__));
$simpletest = '..';
require '../test-settings.php';
require_once $simpletest . '/unit_tester.php';
require_once $simpletest . '/mock_objects.php';

// Easy repository to run tests out ofs
define('GIT_REPO', realpath(dirname(__FILE__) . '/..'));
define('GIT_FIXTURES', realpath(dirname(__FILE__) . '/fixtures'));

require_once '../library/Git.php';

Mock::generate('Git', 'GitMock');

require_once 'GitHarness.php';
require_once 'GitTest.php';
require_once 'Git/TreeTest.php';

$loader = new SimpleFileLoader();
$suite = $loader->createSuiteFromClasses('PHPGit Tests', array(
    'GitTest', 'Git_TreeTest',
));
$result = $suite->run(new DefaultReporter());
if (SimpleReporter::inCli()) {
    exit($result ? 0 : 1);
}
