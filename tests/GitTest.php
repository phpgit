<?php

Mock::generatePartial(
    'Git',
    'GitPartialMock',
    array('execute'));

class GitTest extends UnitTestCase
{
    protected $git;

    public function setUp() {
        $this->git = new GitPartialMock();
    }

    function testTransformArgs() {
        $result = $this->git->transformArgs(array(
            'v' => true,
            's' => 'ours',
            'no-commit' => true,
            'message' => 'test'
        ));
        $expect = array(
            '-v', '-sours', '--no-commit', '--message=test',
        );
        $this->assertIdentical($result, $expect);
    }

    function testCall() {
        $git = new GitPartialMock();
        $git->__construct('dir');
        $git->expectOnce('execute', array(array('git', 'cherry-pick', '-s', 'f533ebca', '--no-commit')));
        $git->setReturnValue('execute', $expect = 'Result');
        $result = $git->cherryPick('f533ebca', array('--no-commit', 's' => true));
        $this->assertIdentical($result, $expect);
    }

    // now, for the ported tests!
    function testCallProcessCallsExecute() {
        //$git = n
    }

}
