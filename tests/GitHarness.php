<?php

class GitHarness extends UnitTestCase
{
    public function fixture($name) {
        return file_get_contents(GIT_FIXTURES . '/' . $name);
    }
}
