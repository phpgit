<?php

class Git_TreeTest extends GitHarness {
    protected $repo, $tree, $git;
    public function setUp() {
        $this->repo = new Git_Repo(GIT_REPO);
        $this->git = $this->repo->git = new GitMock();
        $this->tree = new Git_Tree($this->repo);
    }
    function testContentsShouldCache() {
        $this->git->setReturnValue('__call', $this->fixture('ls_tree_a') . $this->fixture('ls_tree_b'));
        $this->git->expectCallCount('__call', 2);
        $this->git->expectAt(0, '__call', array('lsTree', array('master')));
        $this->git->expectAt(1, '__call', array('lsTree', array('34868e6e7384cb5ee51c543a8187fdff2675b5a7')));
        $tree = $this->repo->tree('master');
        list($child) = array_slice($tree->contents, -1);
        $child->contents;
        $child->contents;
    }
    function testContentFromStringTreeShouldReturnTree() {
        list($text) = array_slice(explode("\n", trim($this->fixture('ls_tree_a'))), -1);
        $tree = $this->tree->contentFromString(null, $text);
        $this->assertIsA($tree, 'Git_Tree');
        $this->assertIdentical('650fa3f0c17f1edb4ae53d8dcca4ac59d86e6c44', $tree->id);
        $this->assertIdentical('040000', $tree->mode);
        $this->assertIdentical('test', $tree->name);
    }
    function testContentFromStringTreeShouldReturnBlob() {
        list($text) = explode("\n", trim($this->fixture('ls_tree_b')));
        $blob = $this->tree->contentFromString(null, $text);
        $this->assertIsA($blob, 'Git_Blob');
        $this->assertIdentical('aa94e396335d2957ca92606f909e53e7beaf3fbb', $blob->id);
        $this->assertIdentical('100644', $blob->mode);
        $this->assertIdentical('grit.rb', $blob->name);
    }
    function testContentFromStringTreeShouldReturnCommit() {
        list($x, $text) = explode("\n", trim($this->fixture('ls_tree_commit')));
        $tree = $this->tree->contentFromString(null, $text);
        $this->assertIdentical($tree, null);
    }
    function testContentFromStringInvalidTypeShouldThrowException() {
        $this->expectException(new Git_Exception('Invalid type: bogus'));
        $this->tree->contentFromString(null, "040000 bogus 650fa3f0c17f1edb4ae53d8dcca4ac59d86e6c44\ttest");
    }
    function testGet() {
        // Python implementation patched blob here, but it doesn't seem
        // to ever be called. Weird. They then go on to test with zero-length 
        // files, which shouldn't have any bearing either. We have condensed
        // those tests.
        $this->git->setReturnValue('__call', $this->fixture('ls_tree_a'));
        $this->git->expectOnce('__call', array('lsTree', array('master')));
        $tree = $this->repo->tree('master');
        $this->assertIdentical('aa06ba24b4e3f463b3c4a85469d0fb9e5b421cf8', $tree->get('lib')->id);
        $this->assertIdentical('8b1e02c0fb554eed2ce2ef737a68bb369d7527df', $tree->get('README.txt')->id);
    }
    function testGetWithCommits() {
        $this->git->setReturnValue('__call', $this->fixture('ls_tree_commit'));
        $this->git->expectOnce('__call', array('lsTree', array('master')));
        $tree = $this->repo->tree('master');
        $this->assertIdentical($tree->get('bar'), null);
        $this->assertIdentical('2afb47bcedf21663580d5e6d2f406f08f3f65f19', $tree->get('foo')->id);
        $this->assertIdentical('f623ee576a09ca491c4a27e48c0dfe04be5f4a2e', $tree->get('baz')->id);
    }
    function testToString() {
        $tree = new Git_Tree($this->repo, array('id' => 'abc'));
        $this->assertIdentical('(Git_Tree "abc")', (string) $tree);
    }
}
