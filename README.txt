phpgit bindings
    (Not to be confused with git-php!)

phpgit is a PHP library for manipulating and retrieving information from
Git repositories.  It wraps the Git executable in a nice manner, and also
has various amounts of syntactical sugar in the form of quick and easy methods
for common operations as well as a full-fledged Tree and Blob representation
format.

Well, at least, in theory. This is a port of GitPython (which itself is a port
of grit), and it is incomplete. Please consult the source code to see which
functions are implemented and which are not.

Also, appropriate credits will be listed eventually here.

Thanks,
Edward

P.S. When talking about this project, please refer to it as "phpgit bindings"
to help disambiguate it against the web-interface, "git-php".
